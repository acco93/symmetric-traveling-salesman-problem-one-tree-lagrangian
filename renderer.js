function draw(problem) {

    var nodes = problem.instance.coords;

    var i;

    var maxX = Number.NEGATIVE_INFINITY;
    var maxY = Number.NEGATIVE_INFINITY;

    var minX = Number.POSITIVE_INFINITY;
    var minY = Number.POSITIVE_INFINITY;


    for (i = 0; i < nodes.length; i++) {

        if (nodes[i].x > maxX) {
            maxX = nodes[i].x;
        }

        if (nodes[i].y > maxY) {
            maxY = nodes[i].y;
        }

        if (nodes[i].x < minX) {
            minX = nodes[i].x;
        }

        if (nodes[i].y < minY) {
            minY = nodes[i].y;
        }
    }

    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");

    // clear the canvas
    ctx.beginPath();
    ctx.clearRect(0, 0, c.width, c.height);


    ctx.font = '12pt Calibri';
    ctx.fillStyle = 'green';
    ctx.fillText("Graph", 20, 20);

    ctx.fillStyle = 'black';

    const GRAPH_MIN_X = 0;
    const GRAPH_MAX_X = 500;
    const GRAPH_MIN_Y = 0;
    const GRAPH_MAX_Y = 500;


    for (i = 0; i < nodes.length; i++) {

        var normalizedX = (nodes[i].x - minX) / (maxX - minX);
        var normalizedY = (nodes[i].y - minY) / (maxY - minY);


        var x = GRAPH_MIN_X + normalizedX * (GRAPH_MAX_X - GRAPH_MIN_X);
        var y = GRAPH_MIN_Y + normalizedY * (GRAPH_MAX_Y - GRAPH_MIN_Y);

        // replace original coordinates
        nodes[i].x = x;
        nodes[i].y = y;

        ctx.fillRect(x, GRAPH_MAX_Y - y, 5, 5);
    }


    for (i = 0; i < problem.oneTree.length - 2; i++) {
        var a = problem.oneTree[i].a;
        var b = problem.oneTree[i].b;
        ctx.moveTo(nodes[a].x, GRAPH_MAX_Y - nodes[a].y);
        ctx.lineTo(nodes[b].x, GRAPH_MAX_Y - nodes[b].y);
        ctx.strokeStyle = '#000000';
        ctx.stroke();
    }


    ctx.beginPath();

    for (i = problem.oneTree.length - 2; i < problem.oneTree.length; i++) {

        var a = problem.oneTree[i].a;
        var b = problem.oneTree[i].b;
        ctx.moveTo(nodes[a].x, GRAPH_MAX_Y - nodes[a].y);
        ctx.lineTo(nodes[b].x, GRAPH_MAX_Y - nodes[b].y);
        ctx.strokeStyle = '#FF0000';
        ctx.stroke();
    }

}
