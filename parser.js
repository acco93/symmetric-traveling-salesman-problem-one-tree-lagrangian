function parseInstance() {

    var coords = retrieveNodesCoordinates();
    var costs = buildCostsMatrix(coords);

    return {
        coords: coords,
        costs: costs,
        zUB: undefined
    }

}

/**
 * Read node coordinates for example from a textarea.
 * NB. a TSPLIB like text must be provided, for now only euclidean distances are supported.
 */
function retrieveNodesCoordinates() {

    // retrieve textarea input and split each line
    var lines = document.getElementById('input').value.split('\n');

    var nodes = [];

    // search for NODE_COORD_SECTION
    for (var i = 0; i < lines.length; i++) {

        var line = lines[i].split(/[ :]+/);
        var sectionName = line[0];


        switch (sectionName.toLowerCase()) {
            case "name":
                break;
            case "comment":
                break;
            case "type":
                break;
            case "dimension":
                break;
            case "edge_weight_type":
                break;
            case "node_coord_section":
                i++;
                while (i < lines.length && lines[i] !== "EOF") {

                    var nodeLine = lines[i].match(/\d+/g).map(Number);

                    nodes.push({
                        x: parseInt(nodeLine[1]),
                        y: parseInt(nodeLine[2])
                    });

                    i++;
                }

                break;
            default:
                break;
        }
    }

    return nodes;

}

/**
 * Build the costs matrix. It is a symmetric matrix then store only the upper triangular matrix to save some space.
 * @param nodes
 *
 */
function buildCostsMatrix(nodes) {

    var costs = new Array(nodes.length * ((nodes.length - 1) / 2));

    var index = 0;

    for (var i = 0; i < nodes.length - 1; i++) {
        for (var j = i + 1; j < nodes.length; j++) {
            var dx = nodes[i].x - nodes[j].x;
            var dy = nodes[i].y - nodes[j].y;
            // compute the euclidean distance
            costs[index] = Math.floor(Math.sqrt((Math.pow(dx, 2.0) + Math.pow(dy, 2.0))) + 0.5);
            index++;
        }
    }

    return costs;
}


