/**
 * Symmetric Traveling Salesman Problem One-Tree Lagrangian Relaxation
 * */

/**
 * Entry point
 */
function compute() {

    // read some lagrangian relaxation parameters
    var alpha = parseInt(document.getElementById('alpha').value);
    var delta = parseInt(document.getElementById('delta').value);
    var maxIterations = parseInt(document.getElementById('maxIterations').value);

    // read some tree search parameters
    var depth = parseInt(document.getElementById('depth').value);

    var parameters = {
        // lagrangian relaxation
        lr: {
            alpha: alpha,
            delta: delta,
            maxIterations: maxIterations
        },
        // tree search
        ts: {
            depth: depth
        }
    };

    treeSearch(parameters);

}

