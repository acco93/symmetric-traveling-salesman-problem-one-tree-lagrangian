/**
 * Problem constructor.
 *
 * @param instance
 * @param costs
 * @param imposedEdges
 * @param initialPenalties
 * @constructor
 */
function Problem(instance, costs, imposedEdges, initialPenalties) {
    // instance reference
    this.instance = instance;
    // number of nodes
    this.n = instance.coords.length;
    // costs upper triangular matrix
    this.costs = costs;
    // edges imposed in the tree search system
    this.imposedEdges = imposedEdges;
    // problem lower bound
    this.lowerBound = undefined;
    // problem one tree
    this.oneTree = undefined;
    // nodes degrees
    this.degrees = undefined;
    // is the lower bound also the optimal solution?
    this.isOptimal = undefined;
    // lagrangian multipliers
    this.penalties = initialPenalties;
    // problem level in the tree search
    this.level = undefined;
}

/**
 * Compute a lower bound to the problem and update the problem state.
 */
Problem.prototype.computeLowerBound = function (maxIterations) {
    var result = lagrangianRelaxation(this, maxIterations);
    this.lowerBound = result.zLB;
    this.oneTree = result.oneTree;
    this.isOptimal = result.optimal;
    this.degrees = result.degrees;
};

/**
 * Compute the linear index of (i,j)
 * @param i
 * @param j
 */
Problem.prototype.indexOf = function (i, j) {
    if (i > j) {
        var tmp = i;
        i = j;
        j = tmp;
    }
    return (this.n * (this.n - 1) / 2) - (this.n - i) * ((this.n - i) - 1) / 2 + j - i - 1;
};

/**
 * Edit the costs using lagrangian multipliers.
 */
Problem.prototype.editCosts = function () {
    var n = this.penalties.length;
    for (var i = 0; i < n; i++) {
        for (var j = i + 1; j < n; j++) {
            var index = this.indexOf(i, j);
            if (this.imposedEdges[index] === null) {
                this.costs[index] = this.instance.costs[index] - this.penalties[i] - this.penalties[j];
            }
        }
    }
};

/**
 * Force an (i,j) arc to be or not be in solution.
 * @param i
 * @param j
 * @param value
 */
Problem.prototype.setImposedEdge = function (i, j, value) {
    this.imposedEdges[this.indexOf(i, j)] = value;
};

/**
 * Check if a specific arc has been forced or not.
 * @param i
 * @param j
 * @returns {*}
 */
Problem.prototype.getImposedEdge = function (i, j) {
    return this.imposedEdges[this.indexOf(i, j)];
};

/**
 * Returns the value in the costs array at index (i,j)
 * @param i
 * @param j
 * @returns {*}
 */
Problem.prototype.getCost = function (i, j) {

    if (i === j) {
        return 0;
    } else {
        return this.costs[this.indexOf(i, j)];
    }

};

/**
 * Edit the cost in position ij
 * @param i
 * @param j
 * @param value
 */
Problem.prototype.setCost = function (i, j, value) {
    this.costs[this.indexOf(i, j)] = value;
};

Problem.prototype.getTab = function () {
    var string = "";
    for (var i = 0; i < this.level; i++) {
        string = string + ".";
    }
    return string;
};