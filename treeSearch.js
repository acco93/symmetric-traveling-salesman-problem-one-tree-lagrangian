function treeSearch(parameters) {

    var i;

    // Parse the instance
    var instance = parseInstance();

    // At the beginning there are no imposed edges
    var initiallyImposedEdges = new Array(instance.costs.length);
    for (i = 0; i < initiallyImposedEdges.length; i++) {
        initiallyImposedEdges[i] = null;
    }

    // Define the initial penalties as 0
    var initialPenalties = new Array(instance.coords.length);
    for (i = 0; i < initialPenalties.length; i++) {
        initialPenalties[i] = 0;
    }

    // Define the initial problem
    var p0 = new Problem(instance, instance.costs.slice(), initiallyImposedEdges, initialPenalties);
    p0.level = 0;

    // Compute an upper bound at the problem, and store it in the instance variable
    // since it will be passed at all the sub-problems
    instance.zUB = computeUpperBound(p0);

    // Store some parameters inside the instance object ... horrible
    instance.parameters = parameters;

    // Compute the lagrangian relaxation on it
    // This will update some problem variables such as:
    // lowerBound, oneTree, degrees, ...
    p0.computeLowerBound(parameters.lr.maxIterations);

    // Define the queue that will contains all the open problems
    var queue = [];

    // Append p0 to the queue
    queue.push(p0);

    var bestProblem = p0;

    var iter = 0;
    var iterWorseLB = 0;
    var inDepth = 10;

    while (queue.length > 0) {

        var p = null;

        if (inDepth > 0) {
            p = queue.pop();
            inDepth--;
        } else {
            queue.sort(function (a, b) {
                return a.lowerBound > b.lowerBound;
            });

            // find the problem with the lowest lower bound, maybe an heap could be useful
            p = queue.shift();
        }


        console.log("i:" + iter + " |Q|:" + queue.length + " " + p.getTab() + " LB:" + p.lowerBound + " UB:" + instance.zUB + " opt:" + p.isOptimal);
        iter++;


        if (p.lowerBound >= instance.zUB) {
            // discard the problem
            iterWorseLB++;

            if (iterWorseLB === 10) {
                inDepth = 10;
                iterWorseLB = 0;
            }

            continue;
        }

        if (p.isOptimal) {
            // p is feasible since it has degrees = 2 for each vertex but it may not be optimal if wrong edges have been imposed
            var cost = computeCost(p);
            if (p.lowerBound < instance.zUB) {
                instance.zUB = p.lowerBound;//computeCost(p);
                bestProblem = p;
            }
        } else {
            // branch

            // find a vertex with degrees > 2
            var v = findVertex(p);

            if (v === null) {
                continue;
            }

            // check if an edge has already been imposed on v
            var alreadyImposed = checkAlreadyImposed(p, v);

            // find two edges not already imposed (by the branching system) that are in the current one-tree solution and link to v
            var edges = findEdges(p, v);

            // retrieve the two associated vertices
            var a = edges.first;
            var b = edges.second;

            if (alreadyImposed === 0) {
                // create the sub-problem where (v,a) and (v,b) are imposed
                var sp1 = createSubProblem(p, v, a, b, 0, alreadyImposed);
                sp1.computeLowerBound(parameters.lr.maxIterations);
                queue.push(sp1);
            }


            var sp2 = createSubProblem(p, v, a, b, 1, alreadyImposed);
            sp2.computeLowerBound(parameters.lr.maxIterations);
            queue.push(sp2);

            var sp3 = createSubProblem(p, v, a, b, 2, alreadyImposed);
            sp3.computeLowerBound(parameters.lr.maxIterations);
            queue.push(sp3);


        }

    }

    draw(bestProblem);
    console.log(bestProblem);
    console.log(computeCost(bestProblem));
}

function computeCost(p) {
    var sum = 0;
    for (var i = 0; i < p.oneTree.length; i++) {
        var index = p.indexOf(p.oneTree[i].a, p.oneTree[i].b);
        sum += p.instance.costs[index];
    }
    return sum;
}

/**
 * Find a vertex with degree > 2
 * @param degrees
 * @returns {*}
 */
function findVertex(problem) {
    for (var i = 0; i < problem.degrees.length; i++) {
        if (problem.degrees[i] > 2 && busyEdges(problem, i) < 2) {
            return i;
        }
    }
    return null;
    throw new Error("Can't happen!");
}

/**
 * Find two edges that have not already been imposed by the branching system and that are in the current one-tree solution and link to vertex
 * @param problem
 * @param vertex
 * @returns {{first: *, second: *}}
 */
function findEdges(problem, vertex) {

    var first = null;
    var second = null;
    var i = 0;

    for (; i < problem.n; i++) {
        if (i !== vertex && problem.getImposedEdge(vertex, i) === null && inSolution(vertex, i, problem.oneTree)) {
            first = i;
            break;
        }
    }
    i++;
    for (; i < problem.n; i++) {
        if (i !== vertex && problem.getImposedEdge(vertex, i) === null && inSolution(vertex, i, problem.oneTree)) {
            second = i;
            break;
        }
    }

    return {
        first: first,
        second: second
    };
}

/**
 * Check if an edge is in solution.
 * @param a
 * @param b
 * @param oneTree
 * @returns {boolean}
 */
function inSolution(a, b, oneTree) {
    // terribly inefficient
    for (var i = 0; i < oneTree.length; i++) {
        if ((oneTree[i].a === a && oneTree[i].b === b) || (oneTree[i].a === b && oneTree[i].b === a)) {
            return true;
        }
    }
    return false;
}

/**
 * Count how many edges have been imposed in a specific vertex. Min: 0, Max: 2
 * @param p
 * @param vertex
 * @returns {number}
 */
function checkAlreadyImposed(p, vertex) {
    var counter = 0;
    for (var i = 0; i < p.n; i++) {
        if (i !== vertex && p.getImposedEdge(vertex, i) === true) {
            counter++;
        }
    }

    if (counter > 2) {
        throw new Error("Error: " + counter + " imposed edges");
    }

    return counter;
}

function createSubProblem(originalProblem, v, a, b, type, alreadyImposed) {

    var pj = new Problem(originalProblem.instance, originalProblem.costs.slice(), originalProblem.imposedEdges.slice(), originalProblem.penalties.slice());
    pj.level = originalProblem.level + 1;

    switch (type) {
        case 0:
            for (var i = 0; i < originalProblem.n; i++) {
                pj.setCost(v, i, Number.POSITIVE_INFINITY);
                pj.setImposedEdge(v, i, false);
            }
            pj.setCost(v, a, 0);
            pj.setCost(v, b, 0);
            pj.setImposedEdge(v, a, true);
            pj.setImposedEdge(v, b, true);
            break;
        case 1:
            if (alreadyImposed === 1) {
                for (var i = 0; i < originalProblem.n; i++) {
                    if (pj.getImposedEdge(v, i) === null) {
                        pj.setCost(v, i, Number.POSITIVE_INFINITY);
                        pj.setImposedEdge(v, i, false);
                    }
                }
            }
            pj.setCost(v, a, 0);
            pj.setCost(v, b, Number.POSITIVE_INFINITY);
            pj.setImposedEdge(v, a, true);
            pj.setImposedEdge(v, b, false);
            break;
        case 2:
            pj.setCost(v, a, Number.POSITIVE_INFINITY);
            pj.setImposedEdge(v, a, false);
            break;
        default:
            throw new Error("Illegal state");
    }

    return pj;

}

function busyEdges(problem, vertex) {
    var count = 0;
    for (var i = 0; i < problem.n; i++) {
        if (problem.getImposedEdge(vertex, i) !== null) {
            count++;
        }
    }
    return count;
}
