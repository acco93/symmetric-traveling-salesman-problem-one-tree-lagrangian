function computeUpperBound(problem) {

    // pick a random permutation and compute a 2-opt

    var array = new Array(problem.n);

    for (var i = 0; i < array.length; i++) {
        array[i] = i;
    }


    array = knuthShuffle(array);

    // compute 2-opt

    do {
        var improved = false;
        for (var i = 0; i <= array.length - 2 && !improved; i++) {
            for (var j = i + 2; j <= array.length - 1 && !improved; j++) {
                if (problem.getCost(array[i], array[j]) + problem.getCost(array[i + 1], array[j + 1]) < problem.getCost(array[i], array[i + 1]) + problem.getCost(array[j], array[j + 1])) {
                    reverse(array, i + 1, j);
                    improved = true;
                }
            }
        }
    } while (improved);


    // compute cost
    var zUB = 0;
    for (var i = 0; i < array.length - 1; i++) {
        zUB += problem.getCost(array[i], array[i + 1]);
    }
    zUB += problem.getCost(array[array.length - 1], array[0]);

    return zUB;

}

function knuthShuffle(array) {
    var m = array.length, t, i;

    // While there remain elements to shuffle…
    while (m) {

        // Pick a remaining element…
        i = Math.floor(Math.random() * m--);

        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    }

    return array;
}

function reverse(array, left, right) {

    var middle = (left + right) / 2;

    var counter = 0;

    for (var i = left; i < middle; i++) {
        var temp = array[i];
        array[i] = array[right - counter];
        array[right - counter] = temp;
        counter++;
    }

    return array;
}