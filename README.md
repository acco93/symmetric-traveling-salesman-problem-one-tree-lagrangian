# README #

Symmetric TSP lower bound computed relaxing in a Lagrangian way the degrees equality constraints and computing a number of one-trees guided by Lagrangian penalties.

Working example [here](http://acco93.altervista.org/opt/index.html).