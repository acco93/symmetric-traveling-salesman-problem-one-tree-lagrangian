/**
 * Compute the lagrangian relaxation.
 */
function lagrangianRelaxation(problem, maxIterations) {

    // number of times a worse lower bound is computed
    // we are minimizing since we are searching for the highest one
    var worseLBcounter = 0;
    // current lower bound
    var bestLB = Number.NEGATIVE_INFINITY;

    // vertex to skip in computing the one-tree, someday it may became variable
    var vertexToSkip = 1;

    // retrieve an upper bound that will be used to compute the sub-gradient step
    var zUB = problem.instance.zUB;

    // is this solution optimal?
    var optimal = false;

    // retrieve some parameters
    var alpha = problem.instance.parameters.lr.alpha;
    var delta = problem.instance.parameters.lr.delta;


    for (var iteration = 0; iteration < maxIterations; iteration++) {

        // compute the mst
        var mst = revisedPrim(problem, vertexToSkip);
        // compute the one tree
        var result = oneTree(problem, mst, vertexToSkip);
        // compute nodes degree
        var degrees = computeVertexDegrees(result.oneTree, problem.n);

        // compute the subgradient norm && penalties sum
        var subGradientNorm = 0;
        var penaltiesSum = 0;

        for (var l = 0; l < degrees.length; l++) {
            // -2 = -b
            subGradientNorm += Math.pow(degrees[l] - 2, 2);

            penaltiesSum += problem.penalties[l];

        }


        var zLB = result.cost + 2 * penaltiesSum;

        // Add the (real lagrangian-based) contribution of the 0-costs arcs
        for (var i = 0; i < result.oneTree.length; i++) {
            var a = result.oneTree[i].a;
            var b = result.oneTree[i].b;
            var index = problem.indexOf(a, b);
            if (problem.imposedEdges[index] === true) {
                zLB += problem.instance.costs[index] - problem.penalties[a] - problem.penalties[b];
            }
        }


        // check if the computed lower bound is worse
        if (zLB < bestLB) {
            worseLBcounter++;
        } else {
            bestLB = zLB;
        }

        // if a worse lower bound is computed delta times, the change the alpha parameter
        // maybe the step was too big
        if (worseLBcounter === delta) {
            alpha /= 2;
            worseLBcounter = 0;
        }

        if (subGradientNorm === 0) {
            optimal = true;
            break;
        }

        var step = alpha * ((zUB - zLB) / (subGradientNorm));


        // update the penalties
        for (var i = 0; i < problem.penalties.length; i++) {
            problem.penalties[i] = problem.penalties[i] - step * (degrees[i] - 2);
        }

        problem.editCosts();

    }

    return {
        oneTree: result.oneTree,
        zLB: Math.round(zLB),
        optimal: optimal,
        degrees: degrees
    };


}

/**
 * Compute the MST using Prim algorithm, skipping the specified vertex.
 * @param problem
 * @param vertexToSkip
 */
function revisedPrim(problem, vertexToSkip) {
    var i;
    var n = problem.n;
    var nodes = new Array(n);

    var mst = [];
    var mstCost = 0;

    for (i = 0; i < n; i++) {
        nodes[i] = {
            cost: Number.POSITIVE_INFINITY,
            parent: null,
            taken: false
        }
    }

    nodes[0].cost = 0;

    for (i = 0; i < n; i++) {

        if (i === vertexToSkip) {
            continue;
        }

        var smallestIndex = null;
        var smallestCost = Number.POSITIVE_INFINITY;

        // extract min
        for (var j = 0; j < n; j++) {
            if (j !== vertexToSkip && !nodes[j].taken && nodes[j].cost < smallestCost) {
                smallestCost = nodes[j].cost;
                smallestIndex = j;
            }
        }

        // why should it be null?
        if (smallestIndex === null) {
            console.log("smallest index == null");
            break;
        }

        nodes[smallestIndex].taken = true;

        mstCost += smallestCost;

        if (nodes[smallestIndex].parent !== null) {
            mst.push({
                a: nodes[smallestIndex].parent,
                b: smallestIndex
            });
        }


        // smallestIndex represents the node with the best cost

        // for each adjacent of smallestIndex
        for (var k = 0; k < n; k++) {
            var cost = problem.getCost(smallestIndex, k);
            if (k !== vertexToSkip && !nodes[k].taken && cost < nodes[k].cost) {
                // can reach this node in a better way, update its values
                nodes[k].cost = cost;
                nodes[k].parent = smallestIndex;
            }
        }

    }

    return {
        mst: mst,
        cost: mstCost
    }

}

/**
 * Add the bests 2 edges from the skipped node.
 * @param problem
 * @param primResult
 * @param skippedVertex
 * @returns {{oneTree: Array, cost: (number|*)}}
 */
function oneTree(problem, primResult, skippedVertex) {

    var firstBestEdgeCost = Number.POSITIVE_INFINITY;
    var firstBestEdgeIndex = null;

    var secondBestEdgeCost = Number.POSITIVE_INFINITY;
    var secondBestEdgeIndex = null;

    for (var i = 0; i < problem.n; i++) {
        if (i === skippedVertex) {
            continue;
        }

        var c = problem.getCost(skippedVertex, i);

        if (c < firstBestEdgeCost) {
            secondBestEdgeCost = firstBestEdgeCost;
            secondBestEdgeIndex = firstBestEdgeIndex;

            firstBestEdgeCost = c;
            firstBestEdgeIndex = i;

        } else if (c < secondBestEdgeCost) {

            secondBestEdgeCost = c;
            secondBestEdgeIndex = i;

        }
    }

    primResult.mst.push({
        a: skippedVertex,
        b: firstBestEdgeIndex
    });
    primResult.mst.push({
        a: skippedVertex,
        b: secondBestEdgeIndex
    });

    primResult.cost += firstBestEdgeCost + secondBestEdgeCost;

    return {
        oneTree: primResult.mst,
        cost: primResult.cost
    }

}

/**
 * Compute the nodes degrees
 * @param oneTree
 * @param n
 * @returns {Array}
 */
function computeVertexDegrees(oneTree, n) {

    var i;
    var d = new Array(n);
    for (i = 0; i < n; i++) {
        d[i] = 0;
    }

    for (i = 0; i < oneTree.length; i++) {
        d[oneTree[i].a]++;
        d[oneTree[i].b]++;
    }

    return d;

}
